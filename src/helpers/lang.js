export function lang_page(lang, page) {
    switch (lang) {
        case'RUS': {
            return RusPageCheck(page)
        }
        case'ENG': {
            return EngPageCheck(page)
        }
        default:{
            return RusPageCheck(page)
        }
    }
}
function RusPageCheck(page) {
    switch (page) {
        case 'login': {
            return {
                login: 'Логин',
                password: 'Пароль'
            }
        }
        case 'navbar': {
            return {
                statistics: 'Статистика',
                buys: 'Покупки',
                users: 'Пользователи',
                settings: 'Настройки',
                exit: 'Выход',

            }
        }
        case 'settings': {
            return {
                title: 'Настройки',
            }
        }
    }
}
function EngPageCheck(page) {
    switch (page) {
        case 'login': {
            return {
                login: 'Login',
                password: 'Password'
            }
        }
        case 'settings': {
            return {
                title: 'Settings',
            }
        }
        case 'navbar': {
            return {
                statistics: 'Statistics',
                buys: 'Buys',
                users: 'Users',
                settings: 'Settings',
                exit: 'Exit',

            }
        }
        default:{
            return {err: 'err'}
        }
    }
}
