export function errors_log(mess, lang){
    switch (lang){
        case 'RUS':{
           return RusErrorsLog(mess)
        }
        case 'ENG':{
            return EngErrorsLog(mess)
        }
        default:{
            return RusErrorsLog(mess)
        }
    }
}
function RusErrorsLog(mess){
    switch (mess){
        case 'LOGIN_OR_PASSWORD_ERROR':{
            return 'Неверный логин или пароль'
        }
        default:{
            return 'Системная ошибка'
        }
    }
}
function EngErrorsLog(mess){
    switch (mess){
        case 'LOGIN_OR_PASSWORD_ERROR':{
            return 'login or password error'
        }
        default:{
            return 'system error'
        }
    }
}