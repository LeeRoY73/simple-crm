import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index';
Vue.use(VueRouter)
function requireAuth (to, from, next) {
  console.log('1')
    if (store.getters.getToken) {
      next();
    }else {
      next('/');
    }
}

const routes = [
  {
    path:'/',
    name:'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path:'/mainPage',
    name:'Dashboard',
    beforeEnter: requireAuth,
    component: () => import('@/components/navbar'),
    children: [
      {
        path:'/dashboard',
        name:'Статистика',
        component: () => import('../views/Dashboard.vue')

      },
      {
        path:'/purchase',
        name:'Покупки',
        component: () => import('../views/Purchase.vue')

      },
      {
        path:'/users',
        name:'Пользователи',
        component: () => import('../views/Users.vue')

      },
      {
        path:'/settings',
        name:'Настройки',
        component: () => import('../views/Settings.vue')

      },
    ]

  }
]

const router = new VueRouter({
  routes
})

export default router
