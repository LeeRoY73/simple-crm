import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        navbarName: '',
        token: '',
        userSettings:{
            menuColor: 'linear-gradient(to right, #11998e, #38ef7d)',
            textColor:'black',
            lang: 'RUS',
        },
    },
    getters: {
        getNavbarName: state => {
            return state.navbarName
        },
        getToken: state => {
            return state.token
        },
        getUserSettings: state => {
            return state.userSettings
        }
    },
    mutations: {
        fillNavbarName: (state, payload) => {
            state.navbarName = payload
        },
        fillToken: (state, payload) => {
            state.token = payload
        },
        removeToken: (state) => {
            state.token = null
        },
        fillUserSettings:(state, payload)=>{
            state.userSettings = payload
        }
    },
    actions: {
        FILL_NAVBAR_NAME: (context, payload) => {
            context.commit('fillNavbarName', payload)
        },
        FILL_TOKEN: (context, payload) => {
            context.commit('fillToken', payload)
        },
        REMOVE_TOKEN: (context) => {
            context.commit('removeToken')
        },
        FILL_USER_SETTINGS:(context) =>{
            context.commit('fillUserSettings')
        }
    }
})
