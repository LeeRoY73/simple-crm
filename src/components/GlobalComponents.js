import button from "@/components/button"
import input from "@/components/input"
const GlobalComponents = {
    install(Vue) {
        Vue.component(button.name, button);
        Vue.component(input.name, input);
    }
};

export default GlobalComponents;